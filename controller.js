var schema = require('./schema')
var prompt = require('prompt');
var fs = require('fs');

module.exports.add_student = (data) =>{
    var obj = JSON.parse(data)
    console.log("obj",obj)
    prompt.get(schema.add_schema, function (err, result) {
        console.log("result",result)
        var student         = new Object()
        student.name        = result.name
        student.password    = result.password
        student.roll_num    = result.roll_num    
        temp = {"name":student.name,"password":student.password,"roll_num":student.roll_num}
        obj.student.push(result)
        console.log("after push ",obj)
        var tempContent = JSON.stringify(obj);
        fs.writeFile('app.txt',tempContent, function(err) {
        if (err) {
        return console.error(err);
        }
        console.log("Data Added Successfully")
        return result
        });
    }); 
}

module.exports.update_student = (data) =>{
    var obj = JSON.parse(data)        
    var flag = 1
    console.log(obj['student'][0])        
    console.log("Enter Roll Number ")
    prompt.get("roll_num", function(err, result) {
        for(var row=0;row<obj['student'].length;row++){
            if(obj['student'][row]['roll_num']==result.roll_num){
                flag=0
                // console.log("inside if ",obj['student'][row])
                prompt.get(schema.update_schema, function (err, result1) {
                    console.log(row)
                    obj['student'][row]['name']        = result1.name
                    obj['student'][row]['password']    = result1.password
                    // // obj['student'][row]['roll_num']    = result.roll_num    
                    // console.log("result value at end ",result1)
                    // obj.student.push(result)
                    // console.log("after push ",obj)
                    var tempContent = JSON.stringify(obj);
                    fs.writeFile('app.txt',tempContent, function(err) {
                    if (err) {
                    return console.error(err);
                    }
                    console.log("Data Updated Successfully")
                    });
                });                             
                break
            }
            
        }if(flag==1){
            console.log("No student Found")
        }
    });    
}

module.exports.delete_student = (data)=>{
    var obj = JSON.parse(data)        
    var flag = 1
    console.log(obj)
    console.log("Enter Roll Number ")
    prompt.get("roll_num", function(err, result) {
        for(var row=0;row<obj['student'].length;row++){
            // console.log(obj['student'][row]['roll_num'])
            if(obj['student'][row]['roll_num']==result.roll_num){
                flag=0
                // console.log("inside if ",obj['student'][row])
                // console.log(obj['student'].splice(row,1))        
                console.log("Found")
                obj['student'].splice(row,1)
                var tempContent = JSON.stringify(obj);
                fs.writeFile('app.txt',tempContent, function(err) {
                if (err) {
                return console.error(err);
                }
                console.log("Data Deleted Successfully")
                });
                break
            }
            
        }if(flag==1){
            console.log("No student Found")                    
        }
    });
}

module.exports.search_student = (data) =>{
    var obj = JSON.parse(data)        
    var flag = 1    
    console.log(obj)
    prompt.get("roll_num", function(err, result) {
        for(var row=0;row<obj['student'].length;row++){
            if(obj['student'][row]['roll_num']==result.roll_num){
                flag=0
                console.log(obj['student'][row])
                break
            }
        }if(flag==1){
            console.log("No student Found")                    
        }        
    });
}