var fs = require('fs');
var prompt = require('prompt');
var controller = require('./controller')
var async = require("async");

console.log("1) Add ")
console.log("2) Update ")
console.log("3) Delete ")
console.log("4) Search ")
console.log("5) Exit ")
outer_loop_flag=false
async.forever(
    function(next){
            prompt.get("option", function(err, result) {                                
                console.log("true")
                if(result.option==1){
                    fs.stat("app.txt",(err, stats)=>{
                        if(err){
                            if(err.errno==-4058){
                                fs.appendFile('app.txt', '{"student":[]}', function (err,data) {
                                    if(err){
                                        console.log("errr",err)
                                    }
                                    else{        
                                        fs.readFile('app.txt', 'utf8', function (err,data) {
                                            if(err){
                                                console.log("errr",err)
                                            }
                                            else{        
                                                console.log("data inside else",data)
                                                controller.add_student(data)                
                                                outer_loop_flag = true
                                            }
                                        });        
                                        // controller.add_student(data)                
                                    }
                                });        
                            }
                        }else{
                            fs.readFile('app.txt', 'utf8', function (err,data) {
                                if(err){
                                    console.log("errr",err)
                                }
                                else if(data){        
                                    console.log("data inside else",data)
                                    result = controller.add_student(data)      
                                    console.log("result value",result)
                                    if(result){
                                        next();
                                    }
                                    // outer_loop_flag = true         
                                    
                                }
                            });        
                        }
                    }) 

                }else if(result.option==2){
                    fs.readFile('app.txt', 'utf8', function (err,data) {
                        if (err) {
                            if(err.errno == -4058){
                                console.log("No data found")
                            }
                            else{
                            return console.log(err);
                            }              
                        }
                        else{
                            controller.update_student(data)
                            outer_loop_flag = true
                        }
                    });
                }
                else if(result.option==3){
                    fs.readFile('app.txt', 'utf8', function (err,data) {
                        if (err) {
                            if(err.errno == -4058){
                                console.log("No data found")
                            }
                            else{
                                return console.log(err);
                            }
                        }
                        else{

                            controller.delete_student(data)
                            outer_loop_flag = true
                        }
                    });
                }    
                else if(result.option==4){
                    fs.readFile('app.txt', 'utf8', function (err,data) {
                        if (err) {
                            if(err.errno == -4058){
                                console.log("No data found")
                            }
                            else{
                                return console.log(err);
                            }
                        }
                        else{
                            controller.search_student(data)
                            outer_loop_flag = true
                        }
                    });
                }
                else if(result.option==5){
                    console.log("End")
                    outer_loop_flag=false
                    // break
                }
                else{
                    console.log("Wrong option selected")
                }
        });
        console.log("outer_loop_flag",outer_loop_flag)
    }
    

)

